from django.db import models

# Create your models here.

class Movie(models.Model):
    title = models.CharField(max_length=100)
    decs = models.TextField()
    year = models.IntegerField()

class Genre(models.Model):
    title = models.CharField(max_length=100)
    name = models.ForeignKey(Movie, on_delete=models.CASCADE, null=True, related_name='genre')

class Hashtag(models.Model):
    title = models.ForeignKey(Movie, on_delete=models.CASCADE, related_name='hashtag')